------------------------------------------------------------
-- Copyright (c) 2016 tacigar. All rights reserved.
------------------------------------------------------------
-- Copyright (c) 2020 IFRFSX.
------------------------------------------------------------
-- Copyleft (Я) 2021-2022 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

local S = maidroid.translator

local timers = maidroid.timers
timers.action_max = tonumber(minetest.settings:get("maidroid_farming_job_time")) or 3
local offline_mode = minetest.settings:get_bool("maidroid_offline_player", true)

-- Core interface functions
local on_start, on_pause, on_resume, on_stop, on_step, is_tool

-- Core extra functions
local plant, mow, to_action
local try_to_make_seeds, select_seed_stack, task
local is_seed, is_plantable_place, is_mowable_place

local wander_core =  maidroid.cores.wander
local to_wander = wander_core.to_wander
local core_path = maidroid.cores.path

local ethereal_plants={}
-- ethereal_plants[plant_name_without_steps]={[seed_name]="steps"}
ethereal_plants["ethereal:strawberry"]={["ethereal:strawberry"]="8"}
ethereal_plants["ethereal:onion"]={["ethereal:wild_onion_plant"]="5"}

local target_plants = {}
local cucina_vegana_mod = minetest.get_modpath("cucina_vegana")

if farming and farming.mod and farming.mod == "redo" then
	for k, v in pairs(farming.registered_plants) do
		--[IFRFSX] replace k to v.crop,this is plant's real name.
		local plant = v.crop .. "_" .. v.steps
		table.insert(target_plants, plant)
	end
else
	target_plants = {
		"farming:cotton_8",
		"farming:wheat_8",
	}
end

for e1,e2 in pairs(ethereal_plants) do
	for name,step in pairs(e2) do
		local plant = e1 .. "_" .. step
		table.insert(target_plants, plant)
	end
end

if cucina_vegana_mod then
	for _, val in ipairs(cucina_vegana.plant_settings.bonemeal_list) do
		table.insert(target_plants, val[1] .. val[2])
	end
end

-- is_plantable_place reports whether maidroid can plant any seed.
is_plantable_place = function(pos, name)
	if minetest.is_protected(pos, name) then
		return false
	end
	local node = minetest.get_node(pos)
	local lpos = vector.add(pos, {x = 0, y = -1, z = 0})
	local lnode = minetest.get_node(lpos)
	return node.name == "air"
		and minetest.get_item_group(lnode.name, "soil") > 1
end

-- is_mowable_place reports whether maidroid can mow.
is_mowable_place = function(pos, name)
	if minetest.is_protected(pos, name) then
		return false
	end
	local node = minetest.get_node(pos)
	for _, plant in pairs(target_plants) do
		if plant == node.name then
			return true
		end
	end

	-- pepper is different as it can be harvested at 3 different states
	if node.name == "farming:pepper_7" then
		return true
	elseif node.name == "farming:pepper_6" and math.random(5) == 1 then
		return true
	elseif node.name == "farming:pepper_5" and math.random(25) == 1 then
		return true
	end
	return false
end

local supports = { farming_beans = "farming:beanpole", farming_grapes = "farming:trellis" }

-- select_seed_stack select the first available seed stack
select_seed_stack = function(self)
	local support, s_stack

	self:set_tool(self.master_selected_tool)
	for i, stack in pairs(self:get_inventory():get_list("main")) do
		if not stack:is_empty() and is_seed(stack:get_name()) then
			support = supports[stack:get_name():gsub(":", "_")]
			if not support or self:get_inventory():contains_item("main", support) then
				self.selected_tool = stack:get_name()
				return
			end
		end
	end

	self.selected_tool = nil

	if try_to_make_seeds(self) then
		self.need_core_selection = true
		return
	end

	if self.state ~= maidroid.states.WANDER then
		to_wander(self)
	end
end

on_start = function(self)
	self.path = nil
	wander_core.on_start(self)
end

on_resume = function(self)
	self.path = nil
	select_seed_stack(self)
	wander_core.on_resume(self)
end

on_stop = function(self)
	self.path = nil
	wander_core.on_stop(self)
end

on_pause = function(self)
	wander_core.on_pause(self)
end

local searching_range = {x = 5, y = 2, z = 5}

task_base = function(self, comparator, action)
	local destination = maidroid.helpers.search_surrounding(self:get_pos(), comparator, searching_range, self.owner_name)
	if destination ~= nil then
		local pos = self:get_pos()
		if vector.distance(pos, destination) < 1 then
			self.destination = destination
			self.action = action
			to_action(self)
			return true
		end

		local path = minetest.find_path(pos, destination, 10, 1, 1, "A*")
		if path ~= nil then
			core_path.to_follow_path(self, path, destination, to_action, action)
			return true
		end
	end
end

task = function(self)
	local stack = nil

	-- Planting
	if self.selected_tool and is_seed(self.selected_tool) and
		task_base(self, is_plantable_place, plant) then
		return
	end
	-- Harvesting
	task_base(self, is_mowable_place, mow)
end

is_seed = function(name)
	if minetest.get_item_group(name, "seed") > 0 then
		return true
	end

	for _, v in pairs(farming.registered_plants) do
		if name == v.seed then
			return true
		end
	end

	for s1,s2 in pairs(ethereal_plants) do
		for n,s in pairs(s2) do
			if name == n then
				return true
			end
		end
	end

	return false
end

local seed_recipes = {
	farming_garlic        = { 12, 8, "farming:garlic_clove",   nil, nil},
	farming_melon_8       = { 24, 4, "farming:melon_slice",    nil, "farming:cutting_board"},
	farming_pineapple     = { 19, 5, "farming:pineapple_ring", "farming:pineapple_top", nil},
	farming_pumpkin_8     = { 24, 4, "farming:pumpkin_slice",  nil, "farming:cutting_board"},
	farming_pepper        = { 99, 1, "farming:peppercorn",     nil, nil },
	farming_pepper_yellow = { 99, 1, "farming:peppercorn",     nil, nil },
	farming_pepper_red    = { 99, 1, "farming:peppercorn",     nil, nil },
	farming_sunflower     = { 19, 5, "farming:seed_sunflower", nil, nil },
}

try_to_make_seeds = function(self)
	local inv = self:get_inventory()
	local stack, name, count, val

	for i, i_stack in pairs(inv:get_list("main")) do
		val = seed_recipes[i_stack:get_name():gsub(":", "_")]

		if val and ( not val[5] or
			inv:contains_item("main", val[5]) ) then
			name = i_stack:get_name()
			break
		end
	end
	if not val then
		return
	end

	stack = inv:remove_item("main", ItemStack(name .. " " .. val[1]))
	count = stack:get_count()
	if count == 0 then
		return
	end
	if val[4] then
		table.insert(stack, ItemStack(val[4] .. " " .. count))
	end
	stack = { ItemStack(val[3] .. " " .. count * val[2]) }
	self:add_items_to_main(stack)
	return true
end

to_action = function(self)
	self.state = maidroid.states.ACT
	self.timers.action = 0
	self.timers.walk = 0
end

local get_plantname = function(itemname)
	-- Test vanilla farming mod
	local mod = itemname:split(":")[1]
	if mod == "farming" then
		if farming and not farming.mod then
			return itemname, 1
		end
		for k, v in pairs(farming.registered_plants) do
			if v.seed == itemname then
				--[IFRFSX] replace k to v.crop.
				return v.crop .. "_1"
			end
		end
	elseif mod == "ethereal" and minetest.get_modpath("ethereal") then
		for e1,e2 in pairs(ethereal_plants) do
			for name,step in pairs(e2) do
				if name == itemname then
					return e1 .. "_1"
				end
			end
		end
	-- Support for cucina_vegana seeds
	elseif cucina_vegana_mod and mod == "cucina_vegana" then
		return itemname:gsub("seed", "1")
	end
end

local place_plant_support = function(self, plantname)
	local support = supports[plantname:gsub(":", "_")]
	if not support then
		return true
	end

	local inv = self:get_inventory()
	local stack = ItemStack(support)
	stack = inv:remove_item("main", stack)
	if stack:get_count() == 0 then
		return false
	end
	return true
end

local freeze_action = function(self, toolname)
	-- Check tool was not removed from inventory
	if not toolname then
		return
	end
	self:set_tool(toolname)
	self.object:set_velocity{x=0, y=0, z=0}
	self:set_animation(maidroid.animation_frames.MINE)
	self:set_yaw_by_direction(vector.subtract(self.destination, self:get_pos()))
end

plant = function(self, dtime)
	if self.timers.action < timers.action_max then
		if self.timers.action == 0 then
			freeze_action(self, self.selected_tool)
		end
		self.timers.action = self.timers.action + dtime
		return
	end

	local plantname, p2 = get_plantname(self.selected_tool)
	if not plantname or not self:get_inventory():contains_item("main", self.selected_tool)
		or not place_plant_support(self, self.selected_tool) then
		to_wander(self, 0, timers.change_dir_max )
		self.need_core_selection = true
		return
	end

	p2 = p2 or minetest.registered_nodes[plantname].place_param2 or 1
	minetest.set_node(self.destination, {name = plantname, param2 = p2})
	minetest.sound_play("default_place_node", {pos = self.destination, gain = 1.0})
	self:get_inventory():remove_item("main", ItemStack(self.selected_tool))
	if not self:get_inventory():contains_item("main", self.selected_tool) then
		self.need_core_selection = true
	else
		self:set_tool(self.master_selected_tool)
	end
	to_wander(self, 0, timers.change_dir_max )
end

mow = function(self, dtime)
	if self.timers.action < timers.action_max then
		if self.timers.action == 0 then
			freeze_action(self, self.master_selected_tool)
		end
		self.timers.action = self.timers.action + dtime
		return
	end
	if not self.selected_tool then
		self.need_core_selection = true
	end

	local destnode = minetest.get_node(self.destination)
	minetest.remove_node(self.destination)
	local stacks = minetest.get_node_drops(destnode.name)

	self:add_items_to_main(stacks)
	to_wander(self, 0, timers.change_dir_max )
end

on_step = function(self, dtime, moveresult)
	self:toggle_entity_jump(dtime, moveresult)

	-- When owner offline mode disabled and if owner didn't login, the maidroid does nothing.
	if offline_mode == false
		and not minetest.get_player_by_name(self.owner_name) then
		return
	end

	-- Pickup surrounding items
	self:pickup_item()

	if self.state == maidroid.states.WANDER then
		wander_core.on_step(self, dtime, moveresult, task, maidroid.criteria.is_fence, true)
	elseif self.state == maidroid.states.PATH then
		maidroid.cores.path.on_step(self, dtime, moveresult)
	elseif self.state == maidroid.states.ACT then
		self.action(self, dtime)
	end
end

is_tool = function(stack, stacks)
	if minetest.get_item_group(stack:get_name(), "hoe") > 0 then
		return true
	end
	return false
end

maidroid.register_core("farming", {
	description	= S("a farmer"),
	on_start	= on_start,
	on_stop		= on_stop,
	on_resume	= on_resume,
	on_pause	= on_pause,
	on_step		= on_step,
	is_tool		= is_tool,
	select_tool	= select_seed_stack,
	walk_max = 2.5 * timers.walk_max,
})
maidroid.new_state("ACT")

-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
