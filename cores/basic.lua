------------------------------------------------------------
-- Copyright (c) 2016 tacigar. All rights reserved.
------------------------------------------------------------
-- Copyleft (Я) 2021-2022 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

local S = maidroid.translator

local on_start, on_pause, on_resume, on_stop, on_step

local wander = maidroid.cores.wander
local follow = maidroid.cores.follow

on_start = function(self)
	wander.on_start(self)
end

on_stop = function(self)
	self.state = maidroid.states.IDLE
	self.object:set_velocity{x=0, y=0, z=0}
	self:set_animation(maidroid.animation_frames.STAND)
end

on_pause = function(self)
	self.state = maidroid.states.IDLE
	self.object:set_velocity{x=0, y=0, z=0}
	self:set_animation(maidroid.animation_frames.SIT)
end

on_step = function(self, dtime, moveresult)
	-- This maidroid is owned but owner not connected.
	local player = minetest.get_player_by_name(self.owner_name)
	if self.owner_name ~= "" and not player then
		return
	end

	if self.owner_name ~= "" then
		self:pickup_item(3.0)
	end

	player = self:get_nearest_player(12, true, "default:goldblock")
	if player == nil then
		if self.state ~= maidroid.states.WANDER then
			self:set_animation(maidroid.animation_frames.WALK)
			self.state = maidroid.states.WANDER
		end
		wander.on_step(self, dtime, moveresult)
		return
	end

	-- emit "love" particles
	if self.owner_name == "" then
		local pos = self:get_pos()
		pos.y = pos.y + 0.8
		minetest.add_particlespawner({
			amount = 1,
			time = 0.1,
			minpos = {x = pos.x - 0.1, y = pos.y, z = pos.z - 0.1},
			maxpos = {x = pos.x + 0.1, y = pos.y, z = pos.z + 0.1},
			minvel = {x = -0.2, y = 0.5, z = -0.2},
			maxvel = {x = 0.3,  y = 1.5, z = 0.3},
			minacc = {x = 0, y = -0.1, z = 0},
			maxacc = {x = 0, y = 0.3, z = 0},
			minexptime = 0.1,
			maxexptime = 0.4,
			minsize = 0.2,
			maxsize = 1.5,
			collisiondetection = false,
			vertical = false,
			texture = "default_gold_lump.png",
			player = player
		})
	end

	self:set_animation(maidroid.animation_frames.WALK)
	self.state = maidroid.states.FOLLOW
	follow.on_step(self, dtime, moveresult, player)
end

-- register a definition of a new core.
maidroid.register_core("basic", {
	description      = S("a wanderer"),
	on_start         = on_start,
	on_stop          = on_stop,
	on_resume        = on_start,
	on_pause         = on_pause,
	on_step          = on_step,
	is_tool          = function(stack, stacklist) return false end,
})

-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
