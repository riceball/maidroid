------------------------------------------------------------
-- Copyright (c) 2016 tacigar. All rights reserved.
------------------------------------------------------------
-- Copyleft (Я) 2021-2022 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

-- Core interface function
local on_step, on_pause, on_resume, on_start

-- Core extra functions
local to_wander

local timers = maidroid.timers

timers.change_dir_max = tonumber(minetest.settings:get("maidroid_change_direction_time")) or 2.5 -- change direction at least every n seconds
timers.walk_max = tonumber(minetest.settings:get("maidroid_max_walk_time")) or 4 -- n seconds max walk time

on_start = function(self)
	to_wander(self, 0, timers.change_dir_max )
	self.us_time = minetest.get_us_time()
	self.object:set_velocity{x=0, y=0, z=0}
end

on_resume = function(self)
	to_wander(self, 0, timers.change_dir_max )
	self.object:set_velocity{x=0, y=0, z=0}
end

on_stop = function(self)
	self.state = nil
	self.timers.walk = 0
	self.timers.change_dir = 0
	self.object:set_velocity{x=0, y=0, z=0}
	self:set_animation(maidroid.animation_frames.STAND)
end

on_pause = function(self)
	self.state = nil
	self.object:set_velocity{x=0, y=0, z=0}
	self:set_animation(maidroid.animation_frames.SIT)
end

on_step = function(self, dtime, moveresult, task, block_func, check_inside)
	-- Walk time over do task or randomly happy jump
	if self.timers.walk >= self.core.walk_max then
		self.timers.walk = 0
		self.timers.change_dir = self.timers.change_dir + dtime
		if task then
			task(self, dtime, moveresult)
		elseif math.random(8) == 1 and self:is_on_ground() then
			self.object:set_velocity(vector.add(self.object:get_velocity(),vector.new(0,math.random(20,32)/10,0)))
		end
	-- Time to change dir
	elseif self.timers.change_dir >= timers.change_dir_max then
		self.timers.walk = self.timers.walk + dtime
		self.timers.change_dir = 0
		self:change_direction_randomly()
	else -- Basic step
		self.timers.walk = self.timers.walk + dtime
		self.timers.change_dir = self.timers.change_dir + dtime

		local velocity = self.object:get_velocity()
		local speed = math.sqrt(velocity.x^2 + velocity.z^2)
		if speed < 0.8 or block_func and self:is_blocked(block_func, check_inside) then
			self:change_direction_randomly(true, true)
			self.timers.change_dir = 0
		elseif math.random(24) == 1 and self:is_on_ground() then
			self.object:set_velocity{x=velocity.x, y=maidroid.jump_velocity, z=velocity.z}
		end
	end
	-- TODO check for water or holes
end

to_wander = function(self, walk, change_dir)
	self.state = maidroid.states.WANDER
	self.timers.walk = walk or 0
	self.timers.change_dir = change_dir or 0
	self:change_direction_randomly()
	self:set_animation(maidroid.animation_frames.WALK)
end

maidroid.register_core("wander", {
	on_start	= on_start,
	on_stop		= on_stop,
	on_resume	= on_resume,
	on_pause	= on_pause,
	on_step		= on_step,
	to_wander	= to_wander,
})
maidroid.new_state("WANDER")

-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
