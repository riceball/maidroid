# Maidroid

<img src=misc/banner.png>

This mod is inspired littleMaidMob mod from another voxel game, it provides maid robots called "maidroid".

These robots automatize some daily in-game tasks depending on the items you give them.

## How to Use

### Recipes

<table>
<tr>
  <th scope="col">Maidroid Egg</th>
  <th scope="col">Capture rod</th>
  <th scope="col">Nametag</th>
</tr>
<tr>
  <td style="background-color: grey;" align="center"><img src="textures/maidroid_maidroid_egg.png"></td>
  <td style="background-color: grey;" align="center"><img src="textures/maidroid_tool_capture_rod.png"></td>
  <td style="background-color: grey;" align="center"><img src="textures/maidroid_tool_nametag.png"></td>
</tr>
</tr>
<tr>
  <td>
    <table>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_coal_block.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_mese_block.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_coal_block.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="textures/maidroid_tool_nametag.png"></td>
        <td style="background-color: grey;"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_bronze_block.png"></td>
        <td style="background-color: grey;"></td>
      </tr>
    </table>
  </td>
  <td>
    <table>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/wool/textures/wool_blue.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_red.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_mese_crystal.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_steel_ingot.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_red.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_steel_ingot.png"></td>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/wool/textures/wool_violet.png"></td>
      </tr>
    </table>
  </td>
  <td>
    <table>
      <tr>
        <td style="background-color: grey;"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/farming/textures/farming_cotton.png"></td>
        <td style="background-color: grey;"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_paper.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_paper.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_paper.png"></td>
      </tr>
      <tr>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_tin_lump.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_black.png"></td>
        <td style="background-color: grey;"><img src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_copper_ingot.png"></td>
      </tr>
    </table>
  </td>
</tr>
</table>

### Usage

1. Create a maidroid egg <img style="background-color: grey;" src="textures/maidroid_maidroid_egg.png"> and use it in world to spawn a maid.
3. Tame maidroid punching it with one gold block <img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_gold_block.png">
4. You can colorize them by punching dyes
<table><tr>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_white.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_grey.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_dark_grey.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_black.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_violet.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_blue.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_cyan.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_dark_green.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_green.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_yellow.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_brown.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_orange.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_red.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_magenta.png"></td>
<td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/dye/textures/dye_pink.png"></td>
<tr></table>

5. They will sit down and take a pause if you punch them with activation item
    + Paper <img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_paper.png"> by default
    + Sugar <img style="background-color: grey;" src="https://notabug.org/TenPlus1/farming/raw/master/textures/farming_sugar.png"> if [farming redo](https://notabug.org/TenPlus1/farming) available
6. To activate open their inventory with right-click and put tool and stuff inside.

<table>
<tr>
<td>Basic</td><td></td><td>Maidroids follow owner or any player when wild, if this player have taming item in hand.<br>
<br>When they don't follow anyone they just wander
<br>Defaut if no other core selected</td>
</tr>
<tr>
<td>Farming</td><td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/farming/textures/farming_tool_diamondhoe.png"></td>
<td>Any item from hoe group select this core.<br>
Maidroid will plant the seeds or make seed from plants if they can.<br>
Maidroid harvests mature plants.<br>
Surrounded their land with fences or xpanes, as they are not jumpable by farmers.</td>
</tr>
<tr>
<td>OCR</td><td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_book_written.png"></td>
<td>Maidroid needs a written book written in their inventory.<br> If the bookname is main, program is read.
<pre><code>start: sleep 1
beep
jump 0.9
jmp start
</code></pre>
</tr>
<tr>
<td>Torcher</td><td><img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_torch_on_floor.png"></td>
<td>Maidroids with torches in inventory will follow a player, and put torch if it is dark.</td>
</tr>
<tr>
<td>Stockbreeder</td>
<td>
<img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/bucket/textures/bucket.png">
<br>
<img style="background-color: grey;" width="16" height="16" src="https://raw.githubusercontent.com/runsy/petz/master/petz/textures/petz_shears.png">
</td>
<td>An empty bucket or shears select this core<br>
Stockbreeder will try to feed milkable animals, and then milk them<br>
If they have shears they will also try to shear sheep<br>
If they also have a sword in inventory they will try to manage poultries population<br>
If they have shovel in inventory they can collect poops</td>
</tr>
</table>

## New features
   + Automatic core selection
   + Paintable with dyes
   + Strong ownership for maidroids
       + You need to tame new maidroids
       + You can view any maidroid content
       + You can only take and put items in your maidroids
       + Maidroid privilege allows to
           + kill any maidroid
           + interact with any maidroids
           + set any maidroid owner with nametag
   + Farming
       + Fields separator can be xpanes or fences
           + through fences maidroid were harvesting plants from nearby fields
           + only in old maidroid version, now it seems better
       + Support for all farming redo crops and plants
           + Craft seeds for many plants: garlic, melon, pepper, pineapple, pumpkin
               + Melon and pumpkin require cutting board in inventory
           + Pepper can be harvested in three states: <img style="background-color: grey;" src="https://notabug.org/TenPlus1/farming/raw/master/textures/crops_pepper.png"> <img style="background-color: grey;" src="https://notabug.org/TenPlus1/farming/raw/master/textures/crops_pepper_yellow.png"> <img style="background-color: grey;" src="https://notabug.org/TenPlus1/farming/raw/master/textures/crops_pepper_red.png">
           + Melons <img style="background-color: grey;" src="https://notabug.org/TenPlus1/farming/raw/master/textures/farming_melon_7.png"> and pumpkins <img style="background-color: grey;" src="https://notabug.org/TenPlus1/farming/raw/master/textures/farming_pumpkin_8.png"> can now harvested
       + Support for cucina_vegana
   + Offline mode: droid continue to work even owner is offline
   + Protected areas support
   + Internationalization
   + Health
       + Status in info
       + Egg bar in maidroid menu
       + Can be healed
           + punching with tin lump <img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_tin_lump.png">
           + punching with mese fragment <img style="background-color: grey;" src="https://gitlab.com/minetest/minetest_game/-/raw/master/mods/default/textures/default_mese_crystal_fragment.png">
           + Or just put those items in inventory for auto-healing
           + Drops all inventory items and some base material on death
   + Older version backward compatiblity
   + Nametag right-click sets owner name
   + Support for pipeworks teleport tubes: overflow management
   + Inventory
       + Sneak click
       + 3D model view


## Code enhancement
   + Behaviors core: deported code from initial cores
       + Follow: still to enhance but bit better than before
       + Wander:
           + Try to go back to spawn point if too far from it
           + Randomly jump when wandering
       + Path: follow path after path_finding
       + More flexibility to extend or change behavior management
   + This version is less cpu greedy than older ones
       + Less pathfinding pathes called
       + Wielded item do not leak and survive server restart, even they were cleaned at restart

More features to come, look at [TODO](TODO)

## Migration guide
If you already installed an old version of maidroid
   1. Before launching edit your minetest.conf
      + set "maidroid_compat = true"
   2. Launch your world and ensure you activate every maidroid on the map or captured egg from inventories
   3. When migration is finished edit your minetest.conf and remove maidroid_compat
   4. For a new install you should set above config
   5. If you used this version you can't go back to old versions
      + no forward compatibility !!!
   6. Original tagicar and forks were modpacks, everything is now contained in a single mod, CDB still provide modpack but with single mod inside.
      + you have to remove load_mod_maidroid_core and load_mod_maidroid_tools from your world.mt, else there will be startup warnings
   7. Cores are now mandatory some will not load when dependency not loaded
   8. You can set `maidroid_enable_capture_rod = false` in your world.mt to disable loading of capture rod

## Dependencies

All dependencies were made optional
+ bucket
+ default
+ dye
+ farming or [farming redo](https://notabug.org/tenplus1/farming)
+ wool
+ [animalia](https://github.com/ElCeejo/animalia)
+ [cucina_vegana](https://github.com/acmgit/cucina_vegana)
+ [ethereal](https://notabug.org/tenplus1/ethereal)
+ [mobs_animal](https://notabug.org/tenplus1/mobs_animal)
+ [pdisc](https://github.com/HybridDog/pdisc): without this mod OCR-reader core will be disabled
+ [petz](https://github.com/runsy/petz)
+ [pipeworks](https://gitlab.com/VanessaE/pipeworks)

## Requirements

- Minetest 5.3.0

## About older versions


This mod fork is based on **Bingfeng version** [Minetest Forum](https://forum.minetest.net/viewtopic.php?f=9&t=25523)
   + [IFRFSX](https://github.com/IFRFSX/bingfeng-maidroid "Github")

   Merged patchset
   + [thePlasm](https://github.com/thePlasm/maidroid "Github")
   + [HybridDog](https://github.com/HybridDog/maidroid "Github")
   + [Desour](https://github.com/Desour/maidroid "Github")

**Original tacigar version** [Minetest Forum](https://forum.minetest.net/viewtopic.php?f=9&t=14808)
   + [tacigar's Origin Version](https://github.com/tacigar/maidroid "Github")

The source code of Maidroid is available under the [LGPLv2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt) or later license.

The resouces included in Maidroid are available under the [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) or later license.
