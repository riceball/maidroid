------------------------------------------------------------
-- Copyright (c) 2016 tacigar. All rights reserved.
------------------------------------------------------------
-- Copyleft (Я) 2021 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

local S = maidroid.translator

local formspec = "size[4,1.25]"
			.. "button_exit[3,0.25;1,0.875;apply_name;" .. S("Apply") .. "]"
			.. "field[0.5,0.5;2.75,1;name;" .. S("name") .. ";%s]"

local formspec_r = "size[4,1.25]"
			.. "button_exit[3,0.25;1,0.875;apply_owner;" .. S("Apply") .. "]"
			.. "field[0.5,0.5;2.75,1;owner;" .. S("Owner") .. ";%s]"


local maidroid_buf = {} -- for buffer of target maidroids.

minetest.register_craftitem(":maidroid_tool:nametag", {
	description      = S("maidroid nametag"),
	inventory_image  = "maidroid_tool_nametag.png",
	stack_max        = 1,

	on_use = function(itemstack, user, pointed_thing)
		if pointed_thing.type ~= "object" then
			return
		end

		local obj = pointed_thing.ref
		local luaentity = obj:get_luaentity()

		if obj:is_player() or not luaentity or not
			( maidroid.is_maidroid(luaentity.name) or luaentity:player_can_control(user) ) then
			return
		end

		local nametag = luaentity.nametag or ""

		minetest.show_formspec(user:get_player_name(), "maidroid_tool:nametag", formspec:format(nametag))
		maidroid_buf[user:get_player_name()] = { object = obj, itemstack = itemstack }
	end,
	on_place = function(itemstack, placer, pointed_thing)
		if pointed_thing.type ~= "object" then
			return
		end

		local obj = pointed_thing.ref
		local luaentity = obj:get_luaentity()

		if obj:is_player() or not luaentity or not
			( maidroid.is_maidroid(luaentity.name) or luaentity:player_can_control(placer) ) then
			return
		end

		minetest.show_formspec(placer:get_player_name(), "maidroid_tool:ownertag", formspec_r:format(luaentity.owner_name))
		maidroid_buf[placer:get_player_name()] = { object = obj, itemstack = placer:get_wielded_item() }
	end,
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
	local player_name = player:get_player_name()
	local success = false

	if formname == "maidroid_tool:nametag" and fields.name then
		local luaentity = maidroid_buf[player_name].object:get_luaentity()
		if luaentity then
			luaentity.nametag = fields.name

			luaentity.object:set_properties{
				nametag = fields.name,
			}
			success = true
		end
	end

	if formname == "maidroid_tool:ownertag" and fields.owner then
		local luaentity = maidroid_buf[player_name].object:get_luaentity()
		if luaentity and minetest.player_exists(fields.owner) then
			luaentity.owner_name = fields.owner
			luaentity:update_infotext()
			success = true
		end
	end

	if success then
		local itemstack = maidroid_buf[player_name].itemstack
		itemstack:take_item()
		player:set_wielded_item(itemstack)
	end

	maidroid_buf[player_name] = nil
end)

-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
