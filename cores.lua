------------------------------------------------------------
-- Copyright (c) 2016 tacigar. All rights reserved.
------------------------------------------------------------
-- Copyleft (Я) 2021-2022 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

maidroid.mods = {}

maidroid.mods.animal   = minetest.get_modpath("mobs_animal")
maidroid.mods.animalia = minetest.get_modpath("animalia")
maidroid.mods.petz     = minetest.get_modpath("petz")

maidroid.jump_velocity = 2.6

-- Helper functions
dofile(maidroid.modpath .. "/cores/helpers.lua")
-- Behaviors cores
dofile(maidroid.modpath .. "/cores/wander.lua") -- Always init first
dofile(maidroid.modpath .. "/cores/path.lua") -- Use to_wander
dofile(maidroid.modpath .. "/cores/follow.lua")

dofile(maidroid.modpath .. "/cores/basic.lua") -- Use wander and follow
-- Jobs cores
if minetest.global_exists("farming") then
	dofile(maidroid.modpath .. "/cores/farming.lua") -- Use: Wander
end
dofile(maidroid.modpath .. "/cores/torcher.lua") -- Use: Follow

if minetest.get_modpath("pdisc") then
	dofile(maidroid.modpath .. "/cores/ocr.lua")
end

-- TODO add support for mobs redo
if maidroid.mods.petz or maidroid.mods.animalia or maidroid.mods.animal then
	dofile(maidroid.modpath .. "/cores/stockbreeder.lua")
end

-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
