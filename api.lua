------------------------------------------------------------
-- Copyright (c) 2016 tacigar. All rights reserved.
------------------------------------------------------------
-- Copyleft (Я) 2021-2022 mazes
-- https://gitlab.com/mazes_80/maidroid
------------------------------------------------------------

local S = maidroid.translator

-- maidroid.animation_frames represents the animation frame data
-- of "models/maidroid.b3d".
maidroid.animation_frames = {
	STAND     = {x =   1, y =  78},
	SIT       = {x =  81, y =  81},
	LAY       = {x = 162, y = 165},
	WALK      = {x = 168, y = 187},
	MINE      = {x = 189, y = 198},
	WALK_MINE = {x = 200, y = 219},
}

-- maidroid.states a table with all known maidroid states
maidroid.states = {}

-- maidroid.helpers a table containing helpers functions
maidroid.helpers = {}

-- local functions
local shift_pos

local maidroid_buf = {} -- for buffer of target maidroids.

-- states counter and function to register a new states
maidroid.states_count = 0
maidroid.new_state = function(string)
	if not maidroid.states[string] then
		maidroid.states_count = maidroid.states_count + 1
		maidroid.states[string] = maidroid.states_count
	end
end

-- maidroid.timers a table containing current timers and max timers value
-- values are initialized in cores
maidroid.timers = {}

-- maidroid.registered_maidroids represents a table that contains
-- definitions of maidroid registered by maidroid.register_maidroid.
maidroid.registered_maidroids = {}

-- maidroid.cores represents a table that contains
-- definitions of core registered by maidroid.register_core.
maidroid.cores = {}

local farming_redo = farming and farming.mod and farming.mod == "redo"
local control_item = "default:paper"
if farming_redo then
	control_item = "farming:sugar"
end

-- maidroid.is_maidroid reports whether a name is maidroid's name.
function maidroid.is_maidroid(name)
	if minetest.settings:get_bool("maidroid_compat", true) then
		if maidroid.registered_maidroids[name] then
			return true
		end
		return false
	end
	return name == "maidroid:maidroid"
end

---------------------------------------------------------------------

-- get_inventory returns a inventory of a maidroid.
local get_inventory = function(self)
	return minetest.get_inventory {
		type = "detached",
		name = self.inventory_name,
	}
end

local set_tube = function(self, tbchannel)
	if not (maidroid.pipeworks and tbchannel ~= "") then
		return
	end

	for keys, tubes in pairs(pipeworks.tptube.get_db()) do
		if tubes["channel"] == self.owner_name .. ";" .. tbchannel then
			self.tbchannel = tbchannel
			self.tube = tubes
		end
	end
end

-- select_tool_for_core: iterate through inventory stacks
-- each core implementing is_tool may get selected if the stack item matches
-- First matching "tool" will be used
local select_tool_for_core = function(self)
	local stacks = self:get_inventory():get_list("main")

	for index, stack in ipairs(stacks) do
		for corename, l_core in pairs(maidroid.cores) do
			if l_core.is_tool and l_core.is_tool(stack, stacks) then
				-- Some core may override default wield item, like farming
				if l_core.select_tool then
					self.master_selected_tool = stack:get_name()
					l_core.select_tool(self)
				else
					self.selected_tool = stack:get_name()
					self:set_tool(stack:get_name())
				end
				return corename
			end
		end
	end

	self:set_tool("maidroid:dummy_empty_craftitem")
	self.selected_tool = nil
	return "basic"
end

-- select_core returns a maidroid's current core definition.
local select_core = function(self)
	local name = select_tool_for_core(self)
	if not self.core or self.core.name ~= name or name == "ocr" then
		if self.core then -- used only when maidroid activated
			self.core.on_stop(self)
		end
		self.core = maidroid.cores[name]
		self.core.on_start(self)
		if self.pause then
			self.core.on_pause(self)
		end
		self:update_infotext()
	end
end

-- set_tool set wield tool image and attach.
local set_tool = function(self, name)
	local p = { x=0.4875, y=2.75, z=-1.125 }
	self.wield_item:set_properties({ wield_item = name })

	if  minetest.get_item_group(name, "hoe") > 0 or
		minetest.get_item_group(name, "sword") > 0 or
		name == "mobs:shears" then
		p.x = p.x + 0.5
	elseif name:split(":")[2] == "shears" then
		p.x = p.x - 1
		p.y = p.y + 1
	end
	self.wield_item:set_attach(self.object, "Arm_R", p, {x=-90, y=0, z=-45})
end

-- get_pos get the position of maidroid object
local get_pos = function(self)
	return self.object:get_pos()
end

-- is_on_ground return true if maidroid touches floor
local is_on_ground = function(self, moveresult)
	if moveresult then
		return moveresult.touching_ground
	end
	local under = minetest.get_node(vector.add(self:get_pos(),vector.new(0,-0.8,0)))
	return maidroid.criteria.is_walkable(under.name)
end

-- get_nearest_player returns a player object who
-- is the nearest to the maidroid.
local get_nearest_player = function(self, range_distance, check_owner, wield_item)
	local player, min_distance = nil, range_distance
	local position = self:get_pos()

	-- Tamed droids follow only their owner
	if check_owner and self.owner_name ~= "" then
		player = minetest.get_player_by_name(self.owner_name)
		if vector.distance(self:get_pos(), player:get_pos()) > range_distance or
			( wield_item and player:get_wielded_item():get_name() ~= wield_item ) then
			return nil
		end
		return player
	end

	local all_objects = minetest.get_objects_inside_radius(position, range_distance)
	for _, object in pairs(all_objects) do
		if object:is_player() and
			( not wield_item or object:get_wielded_item():get_name() == wield_item ) then
			local player_position = object:get_pos()
			local distance = vector.distance(position, player_position)

			if distance < min_distance then
				min_distance = distance
				player = object
			end
		end
	end

	return player
end

local round_direction_axis = function(value)
	if value >= 0.5 then
		return 1
	elseif value <= -0.5 then
		return -1
	end
	return 0
end

-- round_direction rounds a direction to int
-- does not backup data
local round_direction = function(direction)
	direction.x = round_direction_axis(direction.x)
	direction.z = round_direction_axis(direction.z)
end

-- get_front returns a position in front of the maidroid.
local get_front = function(self, offset_coefficient)
	local direction = self:get_look_direction()
	local position = self:get_pos()
	if not offset_coefficient then
		position = vector.round(position)
	else
		position = vector.add(position, vector.multiply(direction, offset_coefficient))
	end
	round_direction(direction)

	return vector.add(position, direction)
end

-- get_front_node returns a node that exists in front of the maidroid.
local get_front_node = function(self)
	local front = self:get_front()
	return minetest.get_node(front)
end

-- get_look_direction returns a normalized vector that is
-- the maidroid's looking direction.
local get_look_direction = function(self)
	local yaw = self.object:get_yaw()
	return vector.normalize{x = -math.sin(yaw), y = 0.0, z = math.cos(yaw)}
end

-- set_animation sets the maidroid's animation.
-- this method is wrapper for self.object:set_animation.
local set_animation = function(self, frame)
	self.object:set_animation(frame, 15, 0)
end

-- set_yaw_by_direction sets the maidroid's yaw
-- by a direction vector.
local set_yaw_by_direction = function(self, direction)
	self.object:set_yaw(math.atan2(direction.z, direction.x) - math.pi / 2)
end

local function tube_flush(self, stacks)
	local inv = self:get_inventory()
	if not self.tube or not inv:contains_item("main", "pipeworks:teleport_tube_1") then
		return
	end

	local f_count -- item counter
	local f_stack -- filter stack
	local f_name  -- filter name
	for j=2,4 do  -- Iterate over filters
		f_stack = inv:get_stack("tube",j)
		f_name = f_stack:get_name()
		if f_name and f_name ~= "" then
			for i=#stacks,1,-1 do -- iterate counterwise to be able to remove content
				if stacks[i]:get_name() == f_name then
					pipeworks.tube_inject_item(self:get_pos(), self.tube, vector.new(1,1,1), stacks[i], self.owner_name)
					table.remove(stacks, i)
				end
			end
			-- Send maximal size stacks in teleport tubes until stacks count is under or equal to maximum
			f_count = f_stack:get_stack_max() + 1
			f_stack:set_count(f_count - 1)
			while inv:contains_item("main", f_name .. " " .. f_count) do
				inv:remove_item("main", f_stack)
				pipeworks.tube_inject_item(self:get_pos(), self.tube, vector.new(1,1,1), f_stack, self.owner_name)
			end
		end
	end
end

-- add_items_to_main adds an item list to main inventory
-- return if an oveflow was detected or not
local add_items_to_main = function(self, stacks)
	local inv = self:get_inventory()
	local leftovers = {}
	for _, stack in ipairs(stacks) do
		stack = inv:add_item("main", stack)
		if stack:get_count() > 0 then
			table.insert(leftovers, stack)
		end
	end

	tube_flush(self, leftovers)
	if #leftovers == 0 then return end

	-- TODO find nearest chest
	local pos = self:get_pos()
	for _, stack in ipairs(leftovers) do
		minetest.add_item(shift_pos(pos), stack)
	end
	if minetest.get_player_by_name(self.owner_name) then
		minetest.chat_send_player(self.owner_name, S("A maidroid located at: ") ..
		minetest.pos_to_string(vector.round(self:get_pos()))
		.. S("; needs to take a rest: inventory full"))
	end
	self.core.on_pause(self)
	self.pause = true
	return true
end

-- is_named reports the maidroid is still named.
local is_named = function(self)
	return self.nametag ~= ""
end

-- has_item_in_main reports whether the maidroid has item.
local has_item_in_main = function(self, pred)
	local inv = self:get_inventory()
	local stacks = inv:get_list("main")

	for _, stack in ipairs(stacks) do
		local itemname = stack:get_name()
		if pred(itemname) then
			return true
		end
	end
end

-- change_direction change direction to destination and velocity vector.
local change_direction = function(self, destination, speed)
	local position = self:get_pos()
	local direction = vector.direction(position, destination)
	direction.y = 0

	if not speed then
		speed = math.random(12,18)/10
	end
	local velocity = vector.multiply(direction, speed)

	self.object:set_velocity(velocity)
	self:set_yaw_by_direction(direction)
end

-- change_direction_randomly change direction randonly.
local change_direction_randomly = function(self, invert, force_invert)
	local yaw = ( math.random(314) - 157 ) / 100
	local direction
	local distance = vector.distance(self:get_pos(), self.home)
	-- select a point closer to home, the more the home is far, the more the offset tend to increase
	if not force_invert and distance > 12 then
		direction = vector.subtract(self.home, self:get_pos())
		--if distance > 20 or direction.y > nnn then
		-- TODO notice we need to launch path_finding
		--end
		yaw = minetest.dir_to_yaw(direction) + ( yaw + self.object:get_yaw() ) / math.random(2,6)
	elseif invert then
		if force_invert then
			yaw = yaw / 2
		end
		yaw = yaw + 3.1415 + self.object:get_yaw()
	end

	direction = minetest.yaw_to_dir(yaw)
	self:set_yaw_by_direction(direction)
	direction = vector.multiply(direction, math.random(12, 18) / 10)
	self.object:set_velocity(direction)
end

-- update_infotext updates the infotext of the maidroid.
local update_infotext = function(self)
	local description
	if self.owner_name == "" then
		description = S("looking for gold")
	else
		description = self.core.description
	end

	local infotext = S("this maidroid is ")
		.. ": " .. description .. "\n" .. S("Health")
		.. ": " .. math.ceil(self.object:get_hp() * 100 / self.hp_max) .. "%\n"

	if self.owner_name ~= "" then
		infotext = infotext .. S("Owner") .. " : " .. self.owner_name
	end
	infotext = infotext .. "\n\n\n\n"

	self.object:set_properties({infotext = infotext})
end

maidroid.criteria = {}

maidroid.criteria.is_fence = function(name)
	return minetest.get_item_group(name, "fence") > 0
			or name:gsub(1,7) == "xpanes:"
			or name:gsub(1,6) == "doors:"
end

maidroid.criteria.is_walkable = function(name)
	return name ~= "air" and minetest.registered_nodes[name]
		and minetest.registered_nodes[name].walkable
end

local is_blocked = function(self, criterion, check_inside)
	local pos = self:get_pos()
	local node
	local dir

	if criterion == nil then
		criterion = maidroid.criteria.is_walkable
	end

	if check_inside then
		dir = vector.multiply(self:get_look_direction(), 0.1875)
		node = minetest.get_node(vector.add(pos, dir))
		if criterion(node.name) then
			return true
		end
	end

	local front = self:get_front()
	dir = vector.subtract(front, vector.round(self:get_pos()))
	if dir.x == 0 or dir.z == 0 then
		node = minetest.get_node(front)
	else
		node = minetest.get_node(vector.add(front,vector.new(dir.x, 0, 0)))
		if not criterion(node.name) then
			return false
		end
		node = minetest.get_node(vector.add(front,vector.new(0, 0, dir.z)))
	end
	return criterion(node.name)
end

---------------------------------------------------------------------

local manufacturing_id = {}

-- generate_unique_manufacturing_id generate an unique id for each activated maidroid
-- perfomance issue appears increasingly while the table is filled up
-- having the "gametime" as a source balances this as the collision space is per time units
local function generate_unique_manufacturing_id()
	local id
	while true do
		id = string.format("%s:%x-%x-%x-%x", minetest.get_gametime(), math.random(1048575), math.random(1048575), math.random(1048575), math.random(1048575))
		if manufacturing_id[id] == nil then
			table.insert(manufacturing_id, { id = true })
			return "maidroid:" .. id
		end
	end
end

--------------------------------------------------------------------

-- register empty item entity definition.
-- this entity may be hold by maidroid's hands.
minetest.register_craftitem("maidroid:dummy_empty_craftitem", {
	wield_image = "maidroid_dummy_empty_craftitem.png",
})

minetest.register_entity("maidroid:dummy_item", {
	static_save = false,
	on_activate = function (self, dtime)
		minetest.log("[ Maidroid ]: found old maidroid:dummy_item, cleaning")
		self.object:remove()
	end
})

minetest.register_entity("maidroid:wield_item", {
	hp_max = 1,
	visual = "wielditem",
	visual_size = {x = 0.1875, y = 0.1875},
	collisionbox = {0, 0, 0, 0, 0, 0},
	physical = false,
	static_save = false,
	on_detach = function(self, parent)
		self.object:remove()
	end
})

---------------------------------------------------------------------

-- maidroid.register_core registers a definition of a new core.
function maidroid.register_core(core_name, def)
	def.name = core_name
	if not def.walk_max then
		def.walk_max = maidroid.timers.walk_max
	end
	maidroid.cores[core_name] = def
end

-- player_can_control return if the interacting player "owns" the maidroid
local player_can_control = function(self, player)
	if player then
		local playName = player:get_player_name()
		return self.owner_name and self.owner_name == playName or minetest.check_player_privs(playName, "maidroid")
	end
end

shift_pos = function(pos)
	return vector.new(pos.x + (math.random(11)-6)/10, pos.y, pos.z + (math.random(11)-6)/10)
end

-- heal: heals a maidroid when punched with an healing item
local heal_items = { default_tin_lump = 1, default_mese_crystal_fragment = 3 }
local heal = function(self, stack)
	local hp = self.object:get_hp()
	if hp >= self.hp_max then
		return stack
	end
	local name = stack:get_name():gsub(":", "_")
	if heal_items[name] and stack:take_item():get_count() == 1 then
		self.object:set_hp(hp + heal_items[name])
		self:update_infotext()
	end
	return stack
end

-- autoheal: checks for heal item in maidroid inventory and use it
local autoheal = function(self)
	local inv = self:get_inventory()
	local hp = self.object:get_hp()
	for name, val in pairs(heal_items) do
		local i_name = name:gsub("_", ":")
		if inv:remove_item("main", ItemStack(i_name)):get_count() == 1 then
			self.object:set_hp(hp + heal_items[name])
			self:update_infotext()
			return
		end
	end
end

-- generate_texture return a string with the maidroid texture
maidroid.generate_texture = function(index)
	local texture_name = "[combine:40x40:0,0=maidroid_base.png"
	local color = index
	if type(index) ~= "string" then
		color = dye.dyes[index][1]
	end
	texture_name = texture_name ..  ":24,32=maidroid_eyes_" .. color .. ".png"
	if color == "dark_green" then
		color = "#004800"
	elseif color == "dark_grey" then
		color = "#484848"
	end
	texture_name = texture_name .. "^(maidroid_hairs.png^[colorize:" .. color .. ":255)"
	return texture_name
end

-- create_inventory return a new inventory.
local function create_inventory(self)
	self.inventory_name = generate_unique_manufacturing_id()
	local inventory = minetest.create_detached_inventory(self.inventory_name, {
		on_put = function(inv, listname, index, stack, player)
			if listname == "main" then
				self.need_core_selection = true
			end
		end,

		allow_put = function(inv, listname, index, stack, player)
			if not self:player_can_control(player) then
				return 0
			end
			if listname == "main" then
				return stack:get_count()
			elseif listname == "tube" then
				stack:set_count(1)
				inv:set_stack(listname, index, stack)
				return 0
			end
			return 0
		end,

		on_take = function(inv, listname, index, stack, player)
			if listname == "main" then
				self.need_core_selection = true
			end
		end,

		allow_take = function(inv, listname, index, stack, player)
			if not self:player_can_control(player) then
				return 0
			end
			if listname == "tube" then
				inv:set_stack(listname, index, ItemStack(""))
				return 0
			end
			return stack:get_count()
		end,

		on_move = function(inv, from_list, from_index, to_list, to_index, count, player)
			if to_list == "main" or from_list == "main" then
				self.need_core_selection = true
			end
		end,

		allow_move = function(inv, from_list, from_index, to_list, to_index, count, player)
			if not self:player_can_control(player) then
				return 0
			end

			if from_list == "tube" then
				inv:set_stack(from_list, from_index, ItemStack())
			elseif to_list == "tube" then
				inv:set_stack(to_list, to_index, ItemStack(inv:get_stack(from_list, from_index):get_name()))
			elseif from_list == "main" and to_list == "main" then
				return count
			end
			return 0
		end,
	})

	inventory:set_size("main", 24)
	inventory:set_size("tube", 3)

	return inventory
end

-- create_formspec_string returns a string that represents a formspec definition.
local function create_formspec_string(self, player_name)
	local form = "size[11,7.4]"
		.. "box[0.2,3.9;2.3,2.7;black]"
		.. "box[0.3,4;2.1,2.5;#343848]"
		-- TODO slow down animation
		.. "model[0.2,4;3,3;3d;maidroid.b3d;" .. minetest.formspec_escape(self.textures[1]) .. ";0,180;false;true;200,219]"
		.. "list[detached:"..self.inventory_name..";main;3,0;8,3;]"
		if self.owner_name == player_name then
			form = form .. "list[current_player;main;3,3.4;8,1;]"
			.. "listring[]"
			.. "list[current_player;main;3,4.6;8,3;8]"
		-- TODO add thumbnails for commercial values
		-- else -- TODO show commercial value of items
		end
		form = form .. "label[0,6.6;" .. S("Health") .. "]"

	-- Eggs bar: health view
	local hp = self.object:get_hp() * 8 / self.hp_max
	for i = 0, 8 do
		if i <= hp then
			form = form .. "item_image[" .. i * 0.3 .. ",7.1;0.3,0.3;maidroid:maidroid_egg]"
		else
			form = form .. "image["      .. i * 0.3 .. ",7.1;0.3,0.3;maidroid_empty_egg.png]"
		end
	end

	if maidroid.pipeworks then
		form = form
			.. "label[0,0.25;" .. S("Teleport filters") .. "]"
			.. "list[detached:"..self.inventory_name..";tube;0,1;3,1;]"
			if self.owner_name == player_name then
				form = form .. "field[1.5,2.35;1.8,1;channel;;" .. self.tbchannel
				.. "];field_close_on_enter[channel;false]"
				.. "label[0,2.25;" .. S("Channel") .. "]"
			end
	end
	return form
end

-- on_activate is a callback function that is called when the object is created or recreated.
local function on_activate(self, staticdata)
	-- parse the staticdata, and compose a inventory.
	if staticdata == "" then
		create_inventory(self)
	else
		-- Clone and remove object if it is an "old maidroid"
		if minetest.settings:get_bool("maidroid_compat", true) then
			local name = self.object:get_luaentity().name
			if name:find("maidroid_mk", 9) then
				minetest.chat_send_all("old maidroid found, need to replace")

				-- Fix old datas
				local data = minetest.deserialize(staticdata)
				data["textures"] = maidroid.generate_texture(tonumber(name:split("k")[2]))
				table.insert(data["inventory"]["main"], data["inventory"]["board"][1])
				table.insert(data["inventory"]["main"], data["inventory"]["wield_item"][1])
				table.remove(data["inventory"],data["inventory"]["board"])
				table.remove(data["inventory"],data["inventory"]["core"])
				table.remove(data["inventory"],data["inventory"]["wield_item"])

				-- Create new format maidroid
				local obj = minetest.add_entity(self:get_pos(), "maidroid:maidroid")
				obj:get_luaentity():on_activate(minetest.serialize(data))
				obj:set_yaw(self.object:get_yaw())

				-- Remove this old maidroid
				self.object:remove()
				return
			end
		end

		-- if static data is not empty string, this object has beed already created.
		local data = minetest.deserialize(staticdata)

		self.nametag = data["nametag"]
		self.owner_name = data["owner_name"]
		self.tbchannel = data["tbchannel"] or ""

		local inventory = create_inventory(self)
		for list_name, list in pairs(data["inventory"]) do
			inventory:set_list(list_name, list)
		end
		if data["textures"] ~= nil and data["textures"] ~= "" then
			self.textures = { data["textures"] }
			self.object:set_properties({textures = { data["textures"] }})
		end
		self.home = data["home"]
	end

	self.object:set_nametag_attributes({ text = self.nametag, color = { a=255, r=96, g=224, b=96 }})
	self.object:set_acceleration{x = 0, y = -10, z = 0}

	-- attach dummy item to new maidroid.
	self.wield_item = minetest.add_entity(self:get_pos(), "maidroid:wield_item", minetest.serialize({state = "new"}) )
	self.wield_item:set_attach(self.object, "Arm_R", {x=0.4875, y=2.75, z=-1.125}, {x=-90, y=0, z=-45})
	if not self.home then
		self.home = self:get_pos()
	end
	self.t_health = minetest.get_gametime()
	set_tube(self, self.tbchannel)
	self.timers = {}

	self:select_core()
end

-- get_staticdata is a callback function that is called when the object is destroyed.
local function get_staticdata(self, captured)
	local inventory = self:get_inventory()
	local data = {
		["nametag"] = self.nametag,
		["owner_name"] = self.owner_name,
		["inventory"] = {},
		["textures"] = self.textures[1],
		["tbchannel"] = self.tbchannel
	}

	-- set lists.
	for list_name, list in pairs(inventory:get_lists()) do
		data["inventory"][list_name] = {}

		for i, item in ipairs(list) do
			data["inventory"][list_name][i] = item:to_string()
		end
	end

	if not captured and minetest.settings:get_bool("maidroid_save_home", true) then
		data.home = self.home
	end

	return minetest.serialize(data)
end

-- pickup_item pickup items placed and put it to main slot.
local function pickup_item(self, radius)
	local pos = self:get_pos()
	local all_objects = minetest.get_objects_inside_radius(pos, radius or 1.0)
	local stacks = {}

	for _, obj in pairs(all_objects) do
		local luaentity = obj:get_luaentity()
		if not obj:is_player() and luaentity
			and luaentity.name == "__builtin:item"
			and luaentity.itemstring ~= "" then
			table.insert(stacks, ItemStack(luaentity.itemstring))
			self.need_core_selection = true
			obj:remove()
		end
	end
	self:add_items_to_main(stacks)
end

-- toggle_entity_jump: forbid "jumping" if maidroid is over an entity
local toggle_entity_jump = function(self, dtime, moveresult)
	local stepheight = self.object:get_properties().stepheight
	-- Do not allow "jumping" when standing on object
	if moveresult.standing_on_object and stepheight ~= 0 then
		self.object:set_properties({ stepheight = 0 })
	elseif moveresult.touching_ground and stepheight == 0 then
		self.object:set_properties({ stepheight = 1.1 })
	end
end

-- on_step is a callback function that is called every delta times.
local function on_step(self, dtime, moveresult)
	if self.need_core_selection then
		self:select_core()
		self.need_core_selection = false
	end

	local t_health = minetest.get_gametime()
	if t_health - self.t_health > 10 then
		self.t_health = t_health
		autoheal(self)
	end

	-- do core method.
	if not self.pause then
		self.core.on_step(self, dtime, moveresult)
	end
end

-- on_rightclick is a callback function that is called when a player right-click them.
local function on_rightclick(self, clicker)
	if self.owner_name == "" then
		return -- Not tamed
	end

	if clicker:get_wielded_item():get_name() == "maidroid_tool:nametag" then
		local item = minetest.registered_items["maidroid_tool:nametag"]
		item:on_place(clicker, { ref = self.object, type = "object" } )
		return -- Show owner formspec
	end

	minetest.show_formspec(
		clicker:get_player_name(),
		"maidroid:gui",
		create_formspec_string(self, clicker:get_player_name())
	)
	maidroid_buf[clicker:get_player_name()] = { self = self }
end

local function on_punch(self, puncher, time_from_last_punch, tool_capabilities, dir, damage)
	local player_controls = self.owner_name == "" or self:player_can_control(puncher)
	local stack = puncher:get_wielded_item()

	-- Tame unowned maidroids with a gold block or a gold pie to be implement with support to mod pie
	if self.owner_name == "" and stack:get_name() == "default:goldblock" then
		minetest.chat_send_player(puncher:get_player_name(), S("This maidroid is now yours"))
		self.owner_name = puncher:get_player_name()
		self:update_infotext()
		stack:take_item()
		puncher:set_wielded_item(stack)
	-- ensure player can control maidroid
	elseif not player_controls then
		return true
	-- Pause maidroids with 'control item'
	elseif stack:get_name() == control_item then
		self.pause = not self.pause
		if self.pause == true then
			self.core.on_pause(self)
		else
			self.core.on_resume(self)
		end

		self:update_infotext()
	-- colorize maidroid accordingly when punched by dye
	elseif minetest.get_item_group(stack:get_name(), "dye") > 0 then
		local color = puncher:get_wielded_item():get_name():sub(5)

		local textures = { maidroid.generate_texture( color ) }
		self.object:set_properties( { textures = textures } )
		self.textures = textures

		stack:take_item()
		puncher:set_wielded_item(stack)
	-- Heal
	elseif stack:get_name() == "default:mese_crystal_fragment"
		or stack:get_name() == "default:tin_lump" then
		stack = self:heal(stack)
		puncher:set_wielded_item(stack)
	-- damage your maidroids if your current item is fleshy
	elseif tool_capabilities.damage_groups.fleshy and
		tool_capabilities.damage_groups.fleshy > 1 then
		self.object:set_hp(math.max(math.max(self.object:get_hp() or 0) - damage, 0))
		if self.object:get_hp() == 0 then
			local pos = self:get_pos()

			for _, stack in pairs(self:get_inventory():get_list("main")) do
				minetest.add_item(shift_pos(pos), stack)
			end
			minetest.add_item(shift_pos(pos), ItemStack("default:bronze_ingot 7"))
			minetest.add_item(shift_pos(pos), ItemStack("default:mese_crystal"))

			minetest.sound_play("maidroid_tool_capture_rod_use", {pos = self:get_pos()})
			minetest.add_particlespawner({
				amount = 20,
				time = 0.2,
				minpos = self:get_pos(),
				maxpos = self:get_pos(),
				minvel = {x = -1.5, y = 2, z = -1.5},
				maxvel = {x = 1.5,  y = 4, z = 1.5},
				minacc = {x = 0, y = -8, z = 0},
				maxacc = {x = 0, y = -4, z = 0},
				minexptime = 1,
				maxexptime = 1.5,
				minsize = 1,
				maxsize = 2.5,
				collisiondetection = false,
				vertical = false,
				texture = "maidroid_tool_capture_rod_star.png",
				player = puncher
			})

			self.wield_item:remove()
			self.object:remove()
			return true
		end
		self:update_infotext()
	end
	return true
end

-- register_maidroid registers a definition of a new maidroid.
local register_maidroid = function(product_name, def)
	maidroid.registered_maidroids[product_name] = def

	-- register a definition of a new maidroid.
	minetest.register_entity(product_name, {
		-- basic initial properties
		hp_max   = 15,
		infotext = "",
		nametag  = "",
		mesh     = def.mesh,
		weight   = def.weight,
		textures = def.textures,

		is_visible   = true,
		physical     = true,
		stepheight   = 1.1,
		visual       = "mesh",
		collisionbox = {-0.25, -0.5, -0.25, 0.25, 0.625, 0.25},
		collide_with_objects = true,
		makes_footstep_sound = true,

		-- extra initial properties
		core = nil,
		pause = false,
		tbchannel = "",
		owner_name = "",
		core_name = nil,
		wield_item = nil,
		selected_tool = nil,
		need_core_selection = false,

		-- callback methods.
		on_activate    = on_activate,
		on_step        = on_step,
		on_rightclick  = on_rightclick,
		on_punch       = on_punch,
		get_staticdata = get_staticdata,

		-- extra methods.
		get_inventory             = get_inventory,
		get_nearest_player        = get_nearest_player,
		get_front                 = get_front,
		get_front_node            = get_front_node,
		get_look_direction        = get_look_direction,
		set_animation             = set_animation,
		set_yaw_by_direction      = set_yaw_by_direction,
		add_items_to_main         = add_items_to_main,
		is_named                  = is_named,
		has_item_in_main          = has_item_in_main,
		change_direction          = change_direction,
		change_direction_randomly = change_direction_randomly,
		update_infotext           = update_infotext,
		player_can_control        = player_can_control,
		pickup_item               = pickup_item,
		select_core               = select_core,
		set_tool                  = set_tool,
		heal                      = heal,
		get_pos                   = get_pos,
		is_on_ground              = is_on_ground,
		is_blocked                = is_blocked,
		toggle_entity_jump        = toggle_entity_jump,
	})

	-- register maidroid egg.
	minetest.register_tool("maidroid:maidroid_egg", {
		description = S("Maidroid Egg"),
		inventory_image = def.egg_image,
		stack_max = 1,

		on_use = function(itemstack, user, pointed_thing)
			if pointed_thing.above == nil then
				return nil
			end
			-- set maidroid's direction.
			local new_maidroid = minetest.add_entity(pointed_thing.above, "maidroid:maidroid")
			new_maidroid:get_luaentity():set_yaw_by_direction(
				vector.subtract(user:get_pos(), new_maidroid:get_pos())
			)
			new_maidroid:get_luaentity().owner_name = ""
			new_maidroid:get_luaentity():update_infotext()

			itemstack:take_item()
			return itemstack
		end,
	})
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
	local player_name = player:get_player_name()
	local self

	if maidroid.pipeworks and formname == "maidroid:gui" and fields.channel then
		self = maidroid_buf[player_name].self
		if not (self and maidroid.is_maidroid(self.name)) then
			self = nil
		end
	end

	if self and self.tbchannel ~= fields.channel then
		set_tube(self, fields.channel)
	end

	maidroid_buf[player_name] = nil
end)

register_maidroid( "maidroid:maidroid", {
	hp_max     = 15,
	weight     = 20,
	mesh       = "maidroid.b3d",
	textures   = { "[combine:40x40:0,0=maidroid_base.png:24,32=maidroid_eyes_white.png" },
	egg_image  = "maidroid_maidroid_egg.png",
})

-- Compatibility with tagicar maidroids
if minetest.settings:get_bool("maidroid_compat", true) then
	for i, row in ipairs(dye.dyes) do
		local product_name = "maidroid:maidroid_mk" .. tostring(i)
		local texture_name = maidroid.generate_texture(i)
		local egg_img_name = "maidroid_maidroid_egg.png"
		register_maidroid(product_name, {
			hp_max     = 15,
			weight     = 20,
			mesh       = "maidroid.b3d",
			textures   = { texture_name },
			egg_image  = egg_img_name,
		})

		minetest.register_alias("maidroid:maidroid_mk" .. i .. "_egg", "maidroid:maidroid_egg")
		minetest.register_alias("maidroid_tool:captured_maidroid_mk" .. i .. "_egg", ":maidroid_tool:captured_maidroid_egg")
	end
end

-- vim: ai:noet:ts=4:sw=4:fdm=indent:syntax=lua
